#!/bin/bash

echo Looking for apache server
docker ps | grep apache_server

if [[ "$?" -eq 1 ]] ; then
    echo Apache server is not runing
    docker ps --all | grep apache_server

    if [[ "$?" -eq 0 ]] ; then
        echo Starting apache server...
        docker start apache_server
        echo done...
    else
        echo Container for apache server does not exist, creating a container
        docker run -d -p 8082:80 --name apache_server -v "$PWD/html":/var/www/html php:7.2-apache
    fi
else
    echo Apache server already running...
fi

if [[ "$1" = "server" ]] ; then
    echo Looking for new upstream changes...
    while :
    do
        update=0
        for dir in $(ls html); do
	    echo "Looking for new changes in $dir"
	    cd html/$dir
	    git fetch --all
	    if [ "$(git rev-list HEAD...origin/master --count)" -ne "0" ]; then
	        echo "New changes found!, fething new changes"
	        git pull origin master
	        update=1
	    else
	        echo "No new changes"
	    fi
	    cd -
        done
        if [[ "$update" -eq "1" ]]; then
	    echo "Stopping apache server..."
	    docker stop apache_server
	    echo "done!."
	    sleep 10
	    echo "Starting apache server..."
	    docker start apache_server
	    echo "done!."
        fi
        sleep 60
    done
fi
